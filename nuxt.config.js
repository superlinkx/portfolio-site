export default {
    mode: 'universal',
    /*
     ** Headers of the page
     */
    head: {
        title: "Alex Holms' Portfolio",
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            {
                hid: 'description',
                name: 'description',
                content: "Alex Holms' Portfolio Site",
            },
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: 'stylesheet',
                href: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap',
            },
        ],
    },
    /**
     * PWA Meta
     */
    meta: {
        nativeUI: true,
    },
    /**
     * PWA Manifest
     */
    manifest: {
        name: 'Portfolio',
        lang: 'en',
    },
    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#222222' },
    /*
     ** Global CSS
     */
    css: ['@/assets/scss/app.scss'],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://bootstrap-vue.js.org/docs/
        ['bootstrap-vue/nuxt', { css: false }],
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/pwa',
    ],
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {},
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {},
    },
    server: {
        host: '0.0.0.0',
    },
}
