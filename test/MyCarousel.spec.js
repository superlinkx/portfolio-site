import { mount, createLocalVue } from '@vue/test-utils'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue'
import MyCarousel from '@/components/MyCarousel.vue'

// Local Vue instance to add BootstrapVue as a dep
const localVue = createLocalVue()
localVue.use(BootstrapVue)

// Helper function to make generating our component easier
function getMountedComponent(Component, propsData) {
    return mount(Component, {
        localVue,
        propsData,
    })
}

describe('MyCarousel', () => {
    test('has a property for accepting slides', () => {
        const testSlides = [
            {
                name: 'Test Slide 1',
                img: 'test',
                srcsetSmall: 'test',
                srcsetMedium: 'test',
                srcsetLarge: 'test',
                slideLink: 'test',
            },
            {
                name: 'Test Slide 2',
                img: 'test',
                srcsetSmall: 'test',
                srcsetMedium: 'test',
                srcsetLarge: 'test',
                slideLink: 'test',
            },
        ]
        const wrapper = getMountedComponent(MyCarousel, {
            slides: testSlides,
        })
        const titles = wrapper.findAll('h3')

        expect(titles.at(0).text()).toBe('Test Slide 1')
        expect(titles.at(1).text()).toBe('Test Slide 2')
    })
    test('properly validates slides array', () => {
        const slides = MyCarousel.props.slides
        expect(slides.validator).toBeInstanceOf(Function)
        expect(slides.validator([])).toBeFalsy()
        expect(slides.validator([{}])).toBeFalsy()
        expect(slides.validator([{ name: 'Test Slide 1' }])).toBeFalsy()
        expect(
            slides.validator([
                {
                    name: 'Test Slide 2',
                    img: 'test',
                    srcsetSmall: 'test',
                    srcsetMedium: 'test',
                    srcsetLarge: 'test',
                    slideLink: 'test',
                },
            ])
        ).toBeTruthy()
    })
})
