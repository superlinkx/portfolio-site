# portfolio-site

> Official superlinkx.dev site

## Getting Started

-   Make sure you have [global-docker](https://github.com/superlinkx/global-docker) setup and running
-   Copy `.env-template` to `.env`. Edit values as needed for your environment
-   Run `docker-compose up` and wait for the services to build

## Git Hooks

We use git hooks for some tooling, such as running prettier to lint our code before pushing.  
Please run the following in the project to use the shared hooks: `git config core.hooksPath .githooks`
