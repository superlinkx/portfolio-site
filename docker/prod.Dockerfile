FROM node:lts-buster-slim

RUN apt-get -y update \
    && apt-get install -y git

RUN yarn global add nuxt

COPY . /srv/portfolio-site

WORKDIR /srv/portfolio-site

RUN yarn install

RUN yarn build

RUN apt-get autoremove -y \
    && apt-get autoclean -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 3000

USER node

CMD ["yarn", "start"]